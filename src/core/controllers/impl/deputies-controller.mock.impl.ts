import { DeputiesController } from '../deputies-controller';
import { Observable } from 'rxjs';
import { PagedQueryParams } from 'src/core/common/paged-query-request';

export class DeputiesControllerMockImpl implements DeputiesController.Spec {
  getDeputies: (
    req: PagedQueryParams,
  ) => Observable<DeputiesController.DTO.GetDeputiesResponse> = (req) =>
    new Observable((obs) =>
      obs.next({
        ...req,
        data: [
          { id: 26, name: 'Deputy 1' },
          { id: 27, name: 'Deputy 2' },
        ],
      }),
    );
}
