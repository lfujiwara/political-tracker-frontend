import { AuthController } from '../auth-controller';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class AuthControllerImpl implements AuthController.Spec {
  protected users = [
    {
      id: 1,
      name: 'John Doe',
      email: 'john@gmail.com',
      username: 'johndoe',
      password: '123456',
    },
  ] as any[];

  register: (
    req: AuthController.DTO.RegisterRequest,
  ) => Observable<{ id: any; name: string; username: string; email: string }> =
    (req) => {
      return new Observable((observer) => {
        if (req.password.length < 8)
          throw new AuthController.Errors.InvalidPasswordError();
        if (this.users.filter((user) => user.email === req.email).length > 0)
          throw new AuthController.Errors.EmailAlreadyInUseError();

        const id = this.users.length + 1;
        this.users.push({
          id,
          name: req.name,
          username: req.username,
          email: req.email,
          password: req.password,
        });
        observer.next({
          id,
          name: req.name,
          username: req.username,
          email: req.email,
        });
      });
    };

  login: (
    req: AuthController.DTO.LoginRequest,
  ) => Observable<{ id: any; name: string; username: string; email: string }> =
    (req) => {
      return new Observable((observer) => {
        const user = this.users.filter(
          (user) =>
            user.username === req.username && user.password === req.password,
        )[0];
        if (!user) {
          throw new AuthController.Errors.InvalidCredentialsError();
        }
        observer.next({
          id: user.id,
          name: user.name,
          username: user.username,
          email: user.email,
        });
      });
    };

  getProfile: (req: any) => Observable<AuthController.DTO.UserDTO> = (req) =>
    this.users.find((u) => u.id === req);
}
