import { Observable } from 'rxjs';
import { PagedQueryParams } from 'src/core/common/paged-query-request';
import { PartiesController } from '../parties-controller';

export class PartiesControllerMockImpl implements PartiesController.Spec {
  getParties: (
    req: PagedQueryParams,
  ) => Observable<PartiesController.DTO.GetPartiesResponseDTO> = (req) =>
    new Observable((obs) =>
      obs.next({
        ...req,
        data: [
          { abbreviation: 'PA' },
          { abbreviation: 'PB' },
          { abbreviation: 'PC' },
          { abbreviation: 'PD' },
        ],
      }),
    );
}
