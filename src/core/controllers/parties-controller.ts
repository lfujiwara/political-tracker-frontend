import {
  PagedQueryParams,
  PagedQueryResult,
} from '../common/paged-query-request';

import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

export namespace PartiesController {
  export namespace DTO {
    type PartyDTO = {
      abbreviation: string;
    };

    export type GetPartiesRequestDTO = PagedQueryParams;
    export type GetPartiesResponseDTO = PagedQueryResult<PartyDTO>;
  }

  export type Spec = {
    getParties: (
      req: DTO.GetPartiesRequestDTO,
    ) => Observable<DTO.GetPartiesResponseDTO>;
  };

  export const SpecToken = new InjectionToken<Spec>('PartiesController.Spec');
}
