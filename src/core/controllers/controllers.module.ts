import { AuthController } from './auth-controller';
import { AuthControllerImpl } from './impl/auth-controller.impl';
import { CommonModule } from '@angular/common';
import { DeputiesController } from './deputies-controller';
import { DeputiesControllerMockImpl } from './impl/deputies-controller.mock.impl';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [CommonModule],
  providers: [
    {
      provide: AuthController.SpecToken,
      useClass: AuthControllerImpl,
    },
    {
      provide: DeputiesController.SpecToken,
      useClass: DeputiesControllerMockImpl,
    },
    {
      provide: DeputiesController.SpecToken,
      useClass: DeputiesControllerMockImpl,
    },
  ],
  exports: [],
})
export class ControllersModule {}
