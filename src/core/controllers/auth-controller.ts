import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

export namespace AuthController {
  export namespace Errors {
    export class BaseError extends Error {
      protected code = 'BaseError';
      protected static code = 'BaseError';
      match = (errorClass: any) => errorClass?.code === this.code;
    }

    export class EmailAlreadyInUseError extends BaseError {
      static code = 'EmailAlreadyInUseError';
      code = EmailAlreadyInUseError.code;
    }

    export class UsernameAlreadyInUseError extends BaseError {
      static code = 'UsernameAlreadyInUseError';
      code = UsernameAlreadyInUseError.code;
    }

    export class InvalidPasswordError extends BaseError {
      static code = 'InvalidPasswordError';
      code = InvalidPasswordError.code;
    }

    export class InvalidCredentialsError extends BaseError {
      static code = 'InvalidCredentialsError';
      code = InvalidCredentialsError.code;
    }
  }

  export namespace DTO {
    export type UserDTO = {
      id: any;
      name: string;
      username: string;
      email: string;
    };

    export type RegisterRequest = {
      name: string;
      username: string;
      email: string;
      password: string;
    };

    export type RegisterResponse = UserDTO;

    export type LoginRequest = {
      username: string;
      password: string;
    };

    export type LoginResponse = UserDTO;
  }

  export type Spec = {
    register: (req: DTO.RegisterRequest) => Observable<DTO.RegisterResponse>;
    login: (req: DTO.LoginRequest) => Observable<DTO.LoginResponse>;
  };

  export const SpecToken = new InjectionToken<Spec>('AuthController.Spec');
}
