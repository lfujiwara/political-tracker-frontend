import {
  PagedQueryParams,
  PagedQueryResult,
} from '../common/paged-query-request';

import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

export namespace DeputiesController {
  export namespace DTO {
    type DeputyDTO = {
      id: any;
      name: string;
    };

    export type GetDeputiesRequest = PagedQueryParams;
    export type GetDeputiesResponse = PagedQueryResult<DeputyDTO>;
  }

  export type Spec = {
    getDeputies: (
      req: DTO.GetDeputiesRequest,
    ) => Observable<DTO.GetDeputiesResponse>;
  };

  export const SpecToken = new InjectionToken<Spec>('DeputiesController.Spec');
}
