import { Inject, Injectable } from '@angular/core';
import { AuthController } from '../controllers/auth-controller';

@Injectable()
export class LoginView {
  formData = {
    username: '',
    password: '',
  };
  isLoggedIn = false;
  isLoading = false;
  userData?: AuthController.DTO.UserDTO;

  constructor(
    @Inject(AuthController.SpecToken)
    private authController: AuthController.Spec,
  ) {}

  login() {
    this.isLoading = true;
    const obs = this.authController.login({
      username: this.formData.username,
      password: this.formData.password,
    });
    obs.subscribe({
      next: (userData) => {
        this.userData = userData;
        this.isLoggedIn = true;
        this.isLoading = false;
      },
    });
    return obs;
  }
}
