import { Inject, Injectable } from '@angular/core';
import { AuthController } from '../controllers/auth-controller';

@Injectable()
export class RegisterView {
  formData: AuthController.DTO.RegisterRequest = {
    name: '',
    username: '',
    email: '',
    password: '',
  };
  userData?: AuthController.DTO.RegisterResponse;
  isRegistered = false;
  isLoading = false;
  error = '';

  constructor(
    @Inject(AuthController.SpecToken)
    private authController: AuthController.Spec,
  ) {}

  register() {
    this.isLoading = true;
    const obs = this.authController.register(this.formData);
    obs.subscribe({
      next: (userData) => {
        this.userData = userData;
        this.isRegistered = true;
        this.isLoading = false;
        this.error = '';
      },
      error: (err) => {
        this.error = err.message;
        this.isLoading = false;
      },
    });
    return obs;
  }
}
