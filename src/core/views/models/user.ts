export namespace UserViewModels {
  export type TUser = {
    id: any;
    name: string;
    email: string;
    monitoredTerms: string[];
  };
}
