export namespace PartyViewModels {
  export type TParty = {
    abbreviation: string;
  };
}
