export namespace DeputyViewModels {
  export type TDeputy = {
    id: number;
    name: string;
    email: string;
    partyAbbreviation: string;
    photoUrl: string;
  };
}
