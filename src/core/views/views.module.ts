import { AuthControllerImpl } from '../controllers/impl/auth-controller.impl';
import { CommonModule } from '@angular/common';
import { LoginView } from './login-view';
import { NgModule } from '@angular/core';
import { RegisterView } from './register-view';

@NgModule({
  imports: [CommonModule],
  providers: [AuthControllerImpl, LoginView, RegisterView],
})
export class ViewsModule {}
