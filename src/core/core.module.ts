import { CommonModule } from '@angular/common';
import { ControllersModule } from './controllers/controllers.module';
import { NgModule } from '@angular/core';
import { ViewsModule } from './views/views.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, ControllersModule, ViewsModule],
  exports: [ViewsModule],
})
export class CoreModule {}
