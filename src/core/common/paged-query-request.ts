export type PagedQueryParams = {
  skip: number;
  take: number;
};

export type PagedQueryResult<T> = {
  data: T[];
} & PagedQueryParams;
