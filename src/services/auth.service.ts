import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {
  userData?: {
    id: any;
    name: string;
    username: string;
    email: string;
  };

  isAuthenticated() {
    return !!this.userData;
  }
}
