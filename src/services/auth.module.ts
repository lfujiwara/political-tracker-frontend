import { AuthService } from './auth.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { isAuthenticatedGuard } from './guards/is-authenticated.guard';

@NgModule({
  imports: [CommonModule],
  providers: [AuthService, isAuthenticatedGuard],
  exports: [],
})
export class AuthModule {}
