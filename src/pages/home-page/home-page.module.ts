import { CommonModule } from '@angular/common';
import { HomePageComponent } from './home-page.component';
import { NgModule } from '@angular/core';

@NgModule({
  declarations: [HomePageComponent],
  imports: [CommonModule],
})
export class HomePageModule {}
