import { AuthController } from 'src/core/controllers/auth-controller';
import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RegisterView } from 'src/core/views/register-view';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss'],
})
export class RegisterPageComponent {
  constructor(
    public readonly view: RegisterView,
    private _snackBar: MatSnackBar,
  ) {}

  register() {
    this.view.register().subscribe({
      error: (err) => {
        if (err.match) {
          if (err.match(AuthController.Errors.EmailAlreadyInUseError)) {
            this._snackBar.open('Email já está em uso.', 'OK', {
              duration: 5000,
            });
          } else if (
            err.match(AuthController.Errors.UsernameAlreadyInUseError)
          ) {
            this._snackBar.open('Nome de usuário já está em uso.', 'OK', {
              duration: 5000,
            });
          } else if (err.match(AuthController.Errors.InvalidPasswordError)) {
            this._snackBar.open('Senha muito fraca.', 'OK', {
              duration: 5000,
            });
          } else {
            this._snackBar.open('Algo de errado aconteceu.', 'OK', {
              duration: 5000,
            });
          }
        }
      },
    });
  }
}
