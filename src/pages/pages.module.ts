import { HomePageModule } from './home-page/home-page.module';
import { LoginPageModule } from './login-page/login-page.module';
import { NgModule } from '@angular/core';
import { RegisterPageModule } from './register-page/register-page.module';

const imports = [RegisterPageModule, LoginPageModule, HomePageModule];
const exports = imports;

@NgModule({ imports, exports })
export class PagesModule {}
