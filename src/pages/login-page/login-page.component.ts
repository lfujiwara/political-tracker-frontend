import { AuthController } from 'src/core/controllers/auth-controller';
import { AuthService } from 'src/services/auth.service';
import { Component } from '@angular/core';
import { LoginView } from 'src/core/views/login-view';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent {
  constructor(
    public view: LoginView,
    private _snackBar: MatSnackBar,
    private _router: Router,
    private _authService: AuthService,
  ) {}

  onSuccessfulLogin(data: any) {
    this._authService.userData = data;
    this._router.navigate(['/home']);
  }

  login() {
    this.view.login().subscribe({
      next: this.onSuccessfulLogin.bind(this),
      error: (err) => {
        if (err.match) {
          if (err.match(AuthController.Errors.InvalidCredentialsError)) {
            this._snackBar.open('Login/Senha inválidos.', 'OK', {
              duration: 5000,
            });
          } else {
            this._snackBar.open('Algo de errado aconteceu.', 'OK', {
              duration: 5000,
            });
          }
        } else {
          this._snackBar.open('Algo de errado aconteceu.', 'OK', {
            duration: 5000,
          });
        }
      },
    });
  }
}
