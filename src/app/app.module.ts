import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { AuthModule } from 'src/services/auth.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from 'src/core/core.module';
import { HomePageComponent } from 'src/pages/home-page/home-page.component';
import { LoginPageComponent } from 'src/pages/login-page/login-page.component';
import { NgModule } from '@angular/core';
import { PagesModule } from 'src/pages/pages.module';
import { RegisterPageComponent } from 'src/pages/register-page/register-page.component';
import { isAuthenticatedGuard } from 'src/services/guards/is-authenticated.guard';

const routes: Routes = [
  {
    component: RegisterPageComponent,
    path: 'register',
  },
  {
    component: LoginPageComponent,
    path: 'login',
  },
  {
    component: HomePageComponent,
    path: 'home',
    canActivate: [isAuthenticatedGuard],
  },
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    AuthModule,
    BrowserModule,
    CoreModule,
    PagesModule,
    RouterModule.forRoot(routes),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
